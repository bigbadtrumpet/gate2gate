// set variables for environment
var express = require('express');
var app = express();
var path = require('path');
var compression = require('compression');
var sm = require('sitemap');

app.use(compression());
app.set('port', (process.env.PORT || 5001));
var sitemap = sm.createSitemap ({
      hostname: 'http://www.gate2gatetrailrun.com',
      cacheTime: 600000,        // 600 sec - cache purge period
      urls: [
        { url: '/',  changefreq: 'daily', priority: 0.3 },
        { url: '/interactive_map',  changefreq: 'daily', priority: 0.3 },
        { url: '/race_day_info',  changefreq: 'daily', priority: 0.3 },
        { url: '/accommodations',  changefreq: 'daily', priority: 0.3 }
      ]
    });

// views as directory for all template files
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs');
// instruct express to server up static assets
app.use(express.static('public'));
app.get('/', function(req, res){
  res.render('pages/index')
});
app.get('/interactive_map', function(req, res){
  res.render('pages/interactive_map')
});
app.get('/accommodations', function(req, res){
  res.render('pages/accommodations');
})
app.get('/race_day_info', function(req, res){
  res.render('pages/race_day_info');
})
app.get('/registration', function(req, res){
  res.redirect(301, '/');
})
app.get('/course_info', function(req, res){
  res.redirect(301, '/');
})
app.get('/dates', function(req, res){
  res.redirect(301, '/');
})
app.get('/sitemap.xml', function(req, res) {
  sitemap.toXML( function (err, xml) {
      if (err) {
        return res.status(500).end();
      }
      res.header('Content-Type', 'application/xml');
      res.send( xml );
  });
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
