function getTimeRemaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  t =t/1000;
  var seconds = Math.floor( t % 60 );
  t= t/60;
  var minutes = Math.floor( t % 60 );
  t=t/60;
  var hours = Math.floor( t % 24 );
  t=t/24;
  var days = Math.floor( t );
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}
function initializeClock(id, endtime){
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');
  function updateClock() {
    var t = getTimeRemaining(endtime);
    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = t.hours;
    minutesSpan.innerHTML = t.minutes;
    secondsSpan.innerHTML = t.seconds;
    if(t.total<=0)
    {
      clearInterval(timeinterval);
      clock.innerHTML = "Registration is now open!";
    }
  }
  updateClock();
  var timeinterval = setInterval(updateClock,1000);
};
var deadline = '2016-04-15T07:00:00-0400';
initializeClock('clockdiv', deadline);
